import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class NavbarComponent implements OnInit {

  @Input()
  public loggedIn;

  @Input()
  public username;

  @Input()
  public id;

  @Output()
  public logout = new EventEmitter<undefined>();

  constructor() { }

  ngOnInit() {
  }

  triggerLogout() {
    this.logout.emit();
  }

}
