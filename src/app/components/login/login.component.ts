import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private readonly auth: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
  }

  triggerLogin(username: string, password: string) {
    this.auth.login(username, password).subscribe(
      (result: any) => {
        this.notificator.success(`Welcome, ${result.fullUser.name}!`);
        this.router.navigate(['home']);
      },
      error => this.notificator.error(error.message),
    );
  }

}
