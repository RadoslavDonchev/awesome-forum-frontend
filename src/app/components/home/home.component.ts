import { Component, OnInit } from '@angular/core';
import { Post } from '../../common/post';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  posts: Post[] = [];

  constructor(
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      data => this.posts = data.posts,
    );
    // console.log(this.posts);
  }

}
