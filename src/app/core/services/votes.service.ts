import { Injectable } from '@angular/core';
import { VoteType } from 'src/app/common/votes-enum';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VotesService {

  constructor(private readonly http: HttpClient,
    ) { }

  public vote(likeOrDislike: VoteType, id: string) {
    return this.http.post<{likeOrDislike: VoteType}>(`http://localhost:3006/posts/${id}/votes`, {likeOrDislike});
  }
  public commentVote(likeOrDislike: VoteType, postId: string, commentId: string) {
    return this.http.post<{likeOrDislike: VoteType}>(`http://localhost:3006/posts/${postId}/comments/${commentId}/votes`, {likeOrDislike});
  }
  }
