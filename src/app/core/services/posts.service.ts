import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../../common/post';
import { tap } from 'rxjs/operators';
import { User } from 'src/app/common/user';
import { VoteType } from 'src/app/common/votes-enum';

export interface UserLike {
  likeOrDislike: VoteType;
  userId: string;
}
@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private readonly http: HttpClient,
    ) { }

  public allPosts(): Observable<{posts: Post[]}> {
    return this.http.get<{posts: Post[]}>('http://localhost:3006/posts');
  }

  public idividualPost(postId: string): Observable<{post: Post}> {
    return this.http.get<{post: Post}>(`http://localhost:3006/posts/${postId}`);
  }

  public createPost(title: string, content: string): Observable<{title, content}> {
    return this.http.post<{title, content}>('http://localhost:3006/posts', {
      title, content
    });
  }

  public updatePost(title: string, content: string, postId: string): Observable<{title, content}> {
    return this.http.put<{title, content}>(`http://localhost:3006/posts/${postId}`, {
      title, content
    });
  }

  public deletePost(postId: string): Observable<{post: Post}> {
    return this.http.delete<{post: Post}>(`http://localhost:3006/posts/${postId}`);
  }

  public idividualPostVotes(postId: string): Observable<UserLike[]> {
    return this.http.get<UserLike[]>(`http://localhost:3006/posts/votes/${postId}`);
  }

}
