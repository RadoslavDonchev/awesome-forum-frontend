import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { of } from 'rxjs';

describe('AuthService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
  const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
      {
        provide: StorageService,
        useValue: storage,
      }
    ]
  }));

  it('should be created', () => {

    const service: AuthService = TestBed.get(AuthService);

    expect(service).toBeTruthy();
  });

  describe('login', () => {

  it('should log the user in', () => {

    http.post.and.returnValue(of({
      token: 'token',
      fullUser: {
        name: 'testName',
        id: 'testId',
        role: 'testRole',
        token: 'testToken',
      },
    }));

    const service: AuthService = TestBed.get(AuthService);

    service.login('name', 'password').subscribe(
      (res) => {
        expect(res.fullUser.name).toBe('testName');
        expect(res.fullUser.id).toBe('testId');
        expect(res.fullUser.role).toBe('testRole');
        expect(res.fullUser.token).toBe('testToken');
      }
    );

  });

  it('should call http.post', () => {
    const service: AuthService = TestBed.get(AuthService);

    http.post.calls.reset();
    service.login('name2', 'password2').subscribe(
      () => expect(http.post).toHaveBeenCalledTimes(1)
    );

  });

  it('should call storage.set 4 times with correct values', () => {
    const service: AuthService = TestBed.get(AuthService);

    storage.set.calls.reset();
    service.login('name3', 'password3').subscribe(
      (res) => {
        expect(storage.set).toHaveBeenCalledTimes(4);
        expect(storage.set).toHaveBeenCalledWith('token', res.token);
        expect(storage.set).toHaveBeenCalledWith('username', res.fullUser.name);
        expect(storage.set).toHaveBeenCalledWith('id', res.fullUser.id);
        expect(storage.set).toHaveBeenCalledWith('role', res.fullUser.role);
      }
    );

  });

  it('should update the subject', () => {

    http.post.and.returnValue(of({
      token: 'token2',
      fullUser: {
        name: 'testName2',
        id: 'testId2',
        role: 'testRole2',
        token: 'testToken2',
      },
    }));

    const service: AuthService = TestBed.get(AuthService);

    service.login('name4', 'password4').subscribe(
      () => {
        service.user$.subscribe(
          (sub) => {
            expect(sub.username).toBe('testName2');
            expect(sub.id).toBe('testId2');
        }
        );
      }
    );

  });

});

describe('logout', () => {

  it(`should change the subject to <null>`, () => {
    const service: AuthService = TestBed.get(AuthService);

    service.logout();

    service.user$.subscribe(
      (sub) => {
        expect(sub.username).toBe(null);
        expect(sub.id).toBe(null);
      }
      );

  });

  it(`should call storage.remove 4 times with correct values`, () => {
    const service: AuthService = TestBed.get(AuthService);
    storage.remove.calls.reset();

    service.logout();

    expect(storage.remove).toHaveBeenCalledTimes(4);
    expect(storage.remove).toHaveBeenCalledWith('token');
    expect(storage.remove).toHaveBeenCalledWith('username');
    expect(storage.remove).toHaveBeenCalledWith('id');
    expect(storage.remove).toHaveBeenCalledWith('role');

  });

});

describe('register', () => {

  it('should call http.post', () => {
    const service: AuthService = TestBed.get(AuthService);

    http.post.calls.reset();
    service.register('name5', 'email5', 'password5').subscribe(
      () => expect(http.post).toHaveBeenCalledTimes(1)
    );

  });

});

});
