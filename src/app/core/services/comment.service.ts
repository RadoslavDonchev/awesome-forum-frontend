
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Comment } from 'src/app/common/comment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(
    private readonly http: HttpClient,
    ) { }

  public allComments(id): Observable<{comments: Comment[]}> {
    return this.http.get<{comments: Comment[]}>(`http://localhost:3006/posts/${id}/comments`);
  }

  public createComment(message: string, postId): Observable<{message}> {
    return this.http.post<{message}>(`http://localhost:3006/posts/${postId}/comments`, {
      message
    });
  }


  public deleteComment(commentId: string, postId: string): Observable<{message}> {
    return this.http.delete<{message}>(`http://localhost:3006/posts/${postId}/comments/${commentId}`);
  }

  public updateComment(message: string, postId: string, commentId: string): Observable<{message}> {
    return this.http.put<{message}>(`http://localhost:3006/posts/${postId}/comments/${commentId}`, {
      message
    });
  }

}
