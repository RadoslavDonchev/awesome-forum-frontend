import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { User } from '../../common/user';
import { StorageService } from './storage.service';
import { catchError, tap } from 'rxjs/operators';
import { NotificatorService } from './notificator.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  result: string[] = null;



  constructor(
    private readonly http: HttpClient,
    private readonly notificator: NotificatorService,
    private readonly storage: StorageService,
    ) { }

    public get userFollowing$() {
      return this.userFollowingSubject$.asObservable();
    }

    async getfollowing(): Promise<any> {
      const token = this.storage.get('token');
      const data = await this.http.get<{following: string[]}>(`http://localhost:3006/api/usersbyId/${this.storage.get('id')}`).toPromise();
      return await data.following;
    }

    private readonly userFollowingSubject$ = new BehaviorSubject<any | null>({following: this.getfollowing()});

  public idividualUser(userId: string): Observable<{user: User}> {
    return this.http.get<{user: User}>(`http://localhost:3006/api/usersbyId/${userId}`);
  }

  public followUser(userId: string): Observable<{user: User}> {
    return this.http.post(`http://localhost:3006/api/users/${userId}/friend`, {}).pipe(
      tap((res: any) => {
        this.userFollowingSubject$.next({following: {__zone_symbol__value: res.following}});
      })
    );
  }

  public unFollowUser(userId: string): Observable<{user: User}> {
    return this.http.delete<{user: User}>(`http://localhost:3006/api/users/${userId}/friend`, {}).pipe(
      tap((res: any) => {
        this.userFollowingSubject$.next({following: {__zone_symbol__value: res.following}});
      })
    );
  }

  public banUser(userId: string, description: string): Observable<{description}> {
    return this.http.put<{description}>(`http://localhost:3006/api/users/${userId}/banstatus`, {description});
  }

  public unBanUser(userId: string): Observable<{user: User}> {
    return this.http.delete<{user: User}>(`http://localhost:3006/api/users/${userId}/banstatus`, {});
  }

  public deleteUser(userId: string): Observable<{user: User}> {
    return this.http.delete<{user: User}>(`http://localhost:3006/api/users/${userId}`, {});
  }
}
