import { TestBed, async, inject } from '@angular/core/testing';
import { CommentService } from './comment.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';


describe('Service: Comment', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{

        provide: HttpClient,
        useValue: http,
      }
      ]
    });
  });

  it('should be created', () => {
    const service: CommentService = TestBed.get(CommentService);
    expect(service).toBeTruthy();
  });

  it('allComments should return an arrey of commen objects', () => {
    const service: CommentService = TestBed.get(CommentService);
    http.get.and.returnValue(of([{
      id: '1'
    }, {
      id: '2'
    }]));
    service.allComments('').subscribe(
      res => {
        expect(res[0].id).toEqual('1');
        expect(res[1].id).toEqual('2');
      }
    );

  });

  it('allComments should call http.get once', () => {
    const service: CommentService = TestBed.get(CommentService);

    http.get.calls.reset();
    service.allComments('').subscribe(
      () => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });


  it('createComment should creat a comment by post id', () => {
    const service: CommentService = TestBed.get(CommentService);
    const comment = {
      message: 'message'
    };
    http.post.and.returnValue(of(comment));
    service.createComment('', comment).subscribe(
      (res: any) => expect(res.message).toEqual('message'));
  }
  );

  it('createComment should call http.post once', () => {
    const service: CommentService = TestBed.get(CommentService);
    const message = {
      message: 'this is the message'
    };

    http.post.calls.reset();
    service.createComment('1', message).subscribe(
      () => expect(http.post).toHaveBeenCalledTimes(1)
    );

  });

  // it('createComment should throw error when receaving invalid arguments', () => {
  //   const service: CommentService = TestBed.get(CommentService);
  //   service.createComment(null, null).subscribe(
  //     (res) => expect(res).toThrowError());
  // });


it('updateComment should return updated comment', () => {
  const service: CommentService = TestBed.get(CommentService);
  const comment = {
    message: 'updated'
  };
  http.put.and.returnValue(of(comment));
  service.updateComment(comment.message, '', '').subscribe(
    (res: any) => expect(res.message).toEqual('updated')
  );
});

it('updateComment should call http.put once', () => {
  const service: CommentService = TestBed.get(CommentService);
  const comment = {
    message: 'updated'
  };

  http.put.calls.reset();
  service.updateComment(comment.message, '', '').subscribe(
    () => expect(http.post).toHaveBeenCalledTimes(1)
  );

});

it('deleteComment should return the deleted comment', () =>{
  const service: CommentService = TestBed.get(CommentService);
  const comment = {
    message: 'deleted'
  };
  http.delete.and.returnValue(of(comment));
  service.deleteComment('', '').subscribe(
    (res: any) => expect(res.message).toEqual('deleted')
  );
});


it('deleteComment should call http.delete once', () => {
  const service: CommentService = TestBed.get(CommentService);
  const comment = {
    message: 'updated'
  };

  http.delete.calls.reset();
  service.deleteComment( '', '').subscribe(
    () => expect(http.delete).toHaveBeenCalledTimes(1)
  );

});




















});
