import { TestBed, async, inject } from '@angular/core/testing';
import { PostsService } from './posts.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('PostsService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [
        {
            provide: HttpClient,
            useValue: http,
        }
      ]
    });
  });

  it('should be created', () => {
    const service: PostsService = TestBed.get(PostsService);
    expect(service).toBeTruthy();
  });

  

  it('Should return all posts', () => {

    http.get.and.returnValue(of([
        {
            id: 'id',
            title: 'title',
            content: 'content',
            author: 'author',
            authorId: 'authorId',
            createdOn: '10',
            updatedOn: '11',
            version: 1,
            likes: 1,
            dislikes: 1,
        }
    ]
    ));
    const service: PostsService = TestBed.get(PostsService);
    service.allPosts().subscribe(
        (res) => {
            expect(res[0].id).toBe('id');
            expect(res[0].title).toBe('title');
            expect(res[0].content).toBe('content');
            expect(res[0].author).toBe('author');
            expect(res[0].authorId).toBe('authorId');
            expect(res[0].createdOn).toBe('10');
            expect(res[0].updatedOn).toBe('11');
            expect(res[0].version).toBe(1); 
            expect(res[0].likes).toBe(1);
            expect(res[0].dislikes).toBe(1);
        }
    );
  });


//   it('Should return a single post', () => {

//     http.get.and.returnValue(of(
//         {
//             id: 'id',
//             title: 'title',
//             content: 'content',
//             author: 'author',
//             authorId: 'authorId',
//             createdOn: '10',
//             updatedOn: '11',
//             version: 1,
//             likes: 1,
//             dislikes: 1,
//         }
    
//     ));
//     const service: PostsService = TestBed.get(PostsService);
//     service.idividualPost('id').subscribe(
//         (res) => {
//             expect(res.post.id).toEqual('id');
//             expect(res.post.title).toEqual('title');
//             expect(res.post.content).toEqual('content');
//             expect(res.post.author).toEqual('author');
//             expect(res.post.authorId).toEqual('authorId');
//             expect(res.post.createdOn).toEqual('10');
//             expect(res.post.updatedOn).toEqual('11');
//             expect(res.post.version).toEqual(1); 
//             expect(res.post.likes).toEqual(1);
//             expect(res.post.dislikes).toEqual(1);
//         }
//     );
//   });

  it('IndividualPost should be called once', () => {

    const service: PostsService = TestBed.get(PostsService);
    http.get.calls.reset();
    service.idividualPost('22').subscribe(
        (res) => {
            expect(http.get).toHaveBeenCalledTimes(1);
        }
    );
  });

  it('createPost should create a post', () => {
    const service: PostsService = TestBed.get(PostsService);
    const post = {
      title: 'title',
      content: 'content'
    }

    http.post.and.returnValue(of(post));
    service.createPost(post.title, post.content).subscribe(
      (res: any) =>{
           expect(res.title).toEqual('title');
           expect(res.content).toEqual('content');
      });
  }
  );

  it('createPost should be called once', () => {
    const service: PostsService = TestBed.get(PostsService);

    http.post.calls.reset();
    service.createPost('title', 'content').subscribe(
      (res: any) =>{
           expect(http.post).toHaveBeenCalledTimes(1);
      });
  }
  );

  it('updatePost should update a post', () => {
    const service: PostsService = TestBed.get(PostsService);
    const post = {
      title: 'title',
      content: 'content'
    }

    http.put.and.returnValue(of(post));
    service.updatePost('title', 'content', '23').subscribe(
      (res: any) =>{
           expect(res.title).toEqual('title');
           expect(res.content).toEqual('content');
      });
  }
  );

  it('updatePost should be called once', () => {
    const service: PostsService = TestBed.get(PostsService);

    http.put.calls.reset();
    service.updatePost('title', 'content', '22').subscribe(
      (res: any) =>{
           expect(http.put).toHaveBeenCalledTimes(1);
      });
  }
  );

  it('deletePost should delete a post', () => {
    const service: PostsService = TestBed.get(PostsService);
    const post = {
      title: 'title',
      content: 'content'
    }

    http.delete.and.returnValue(of(post));
    service.deletePost('23').subscribe(
      (res: any) =>{
           expect(res.title).toEqual('title');
           expect(res.content).toEqual('content');
      });
  }
  );

  it('deletePost should be called once', () => {
    const service: PostsService = TestBed.get(PostsService);

    http.delete.calls.reset();
    service.deletePost('22').subscribe(
      (res: any) =>{
           expect(http.delete).toHaveBeenCalledTimes(1);
      });
  }
  );
  
});
