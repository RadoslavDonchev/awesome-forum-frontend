import { Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import { NotificatorService } from './core/services/notificator.service';
import { AuthService } from './core/services/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit, OnDestroy {

  public username = '';
  public isLogged = false;
  public id = '';
  private subscription: Subscription;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly auth: AuthService,
    private readonly router: Router,
  ) {
  }

  ngOnInit() {
    this.subscription = this.auth.user$.subscribe(
      user => {
        if (user.username === null) {
          this.username = '';
          this.isLogged = false;
        } else {
          this.username = user.username;
          this.isLogged = true;
          this.id = user.id;
        }
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['home']);
    this.notificator.success(`You have logged out.`);
  }
}
