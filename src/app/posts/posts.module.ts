import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { PostComponent } from './idividual-post/post.component';
import { PostsComponent } from './all-posts/posts.component';
import { SharedModule } from '../shared/shared.module';
import { MatPaginatorModule, MatSortModule, MatCardModule } from '@angular/material';
import { SheetComponent } from './create-post/sheet/sheet.component';
import { FormsModule } from '@angular/forms';
import { CommentViewComponent } from './comments/comment-view/comment.view.component';
import { CommentSheetComponent } from './idividual-post/create-comment/commentSheet/commentSheet.component';
import { UpdatePostSheetComponent } from './idividual-post/update-post/update-post-sheet/update-post-sheet.component';
import { UpdateCommentSheetComponent } from './idividual-post/update-comment/update-comment-sheet/update-comment-sheet.component';

@NgModule({
  declarations: [
    PostComponent,
    PostsComponent,
    SheetComponent,
    CommentViewComponent,
    CommentSheetComponent,
    UpdatePostSheetComponent,
    UpdateCommentSheetComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    PostsRoutingModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule
  ],
  entryComponents: [
    SheetComponent,
    CommentSheetComponent,
    UpdatePostSheetComponent,
    UpdateCommentSheetComponent,
  ],
})
export class PostsModule { }
