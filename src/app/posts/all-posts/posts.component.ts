import {Component, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Post } from 'src/app/common/post';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort, MatBottomSheet, MatSortable, Sort } from '@angular/material';
import { SheetComponent } from '../create-post/sheet/sheet.component';

/**
 * @title Table with expandable rows
 */
@Component({
  selector: 'app-all-posts',
  styleUrls: ['posts.component.css'],
  templateUrl: 'posts.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PostsComponent implements OnInit {
  columnsToDisplay = ['title', 'author', 'likes', 'dislikes', 'createdOn'];

  public posts: Post[] = [];
  public dataSource: MatTableDataSource<Post>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('poststable') table;

  constructor(
    private readonly route: ActivatedRoute,
    private _bottomSheet: MatBottomSheet,
  ) {
    setTimeout(() => this.dataSource.paginator = this.paginator);
    setTimeout(() => this.dataSource.sort = this.sort);
   }

   applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

   stopPropagation(event: Event) {
    event.stopPropagation();
  }

   public openBottomSheet(): void {
    const sheet = this._bottomSheet.open(SheetComponent, {
      panelClass: 'custom-width'
    });

    sheet.afterDismissed().subscribe( (post: Post) => {
      this.posts.push(post);
      this.dataSource = new MatTableDataSource<Post>(this.posts);
      this.dataSource.sort = this.sort;
      setTimeout(() => this.dataSource.paginator = this.paginator);
      setTimeout(() => this.dataSource.sort = this.sort);
    });
  }

  ngOnInit() {
    this.route.data.subscribe(
      data => {
        this.posts = data.posts;
        this.dataSource = new MatTableDataSource<Post>(this.posts);
        const sortState: Sort = {active: 'createdOn', direction: 'desc'};
        this.sort.active = sortState.active;
        this.sort.direction = sortState.direction;
        this.sort.sortChange.emit(sortState);
      }
    );

    
  }
}
