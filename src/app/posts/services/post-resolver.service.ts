import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { PostsService } from '../../core/services/posts.service';
import { Post } from '../../common/post';

@Injectable({
  providedIn: 'root'
})
export class PostResolverService implements Resolve<{post: Post} | any> {

  constructor(
    private readonly postsService: PostsService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const id = route.params['id'];

    return this.postsService.idividualPost(id)
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of(res);
        }
      ));
  }
}
