import { CommentService } from './../../../app/core/services/comment.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
@Injectable({
  providedIn: 'root'
})
export class CommentsResolverService implements Resolve<{comments: Comment[]}> {

  constructor(
    private readonly commentService: CommentService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const id = route.params['id'];
    return this.commentService.allComments(id)
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of(res);
        }
      ));
  }
}
