import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../../common/comment';
import { VoteType } from '../../../common/votes-enum';
import { VotesService } from '../../../core/services/votes.service';

@Component({
  selector: 'app-comment-view',
  templateUrl: './comment.view.component.html',
  styleUrls: ['./comment.view.component.css']
})

export class CommentViewComponent implements OnInit {
  @Input()
  public postId: string ;
  @Input()
  public comment: Comment;


  @Output()
  public vote = new EventEmitter<{id: string, VoteType: VoteType}>();

  @Output()
  public update = new EventEmitter<{id: string, message: string}>();

  @Output()
  public delete = new EventEmitter<string>();

  @Input()
  public showCommentButtonsDeleteAndUpdate = false;

  private commentId = null;

  constructor(private readonly voteService: VotesService
  ) {

  }


  public deleteComment() {
    this.delete.emit(this.commentId);
  }

  ngOnInit() {
    this.commentId = this.comment.id;
  }

  voteEmmit(vote: VoteType) {
    this.vote.emit({id: this.comment.id, VoteType: vote});
  }

  updateEmmit() {
    this.update.emit({id: this.comment.id, message: this.comment.message});
  }
}
