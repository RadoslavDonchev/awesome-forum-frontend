import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { PostsService } from 'src/app/core/services/posts.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';


@Component({
  selector: 'app-update-post-sheet',
  templateUrl: './update-post-sheet.component.html',
  styleUrls: ['./update-post-sheet.component.css']
})
export class UpdatePostSheetComponent implements OnInit {

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  private _bottomSheetRef: MatBottomSheetRef<UpdatePostSheetComponent>,
  private readonly postService: PostsService,
  private readonly notificator: NotificatorService) {}

  public updatePost(title, content) {
    this.postService.updatePost(title, content, this.data.postId).subscribe(
      (res: any) => {
        this.notificator.success('You have successfully updated the post!');
        this._bottomSheetRef.dismiss(res);
      },
      () => {
        this.notificator.error('The post failed to be updated!');
        this._bottomSheetRef.dismiss();
      }
    );
  }

  public dismiss(): void {
    this._bottomSheetRef.dismiss();
  }

  ngOnInit() {
  }

}
