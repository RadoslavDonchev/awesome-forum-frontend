import { Component, Inject, OnInit } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material';
import { PostsService } from 'src/app/core/services/posts.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { CommentService } from 'src/app/core/services/comment.service';
import { ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { log } from 'util';

@Component({
  selector: 'app-commentSheet',
  templateUrl: './commentSheet.component.html',
  styleUrls: ['./commentSheet.component.css']
})
export class CommentSheetComponent implements OnInit {

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  private _bottomSheetRef: MatBottomSheetRef<CommentSheetComponent>,
  private readonly route: ActivatedRoute,
  private readonly commentService: CommentService,
  private readonly notificator: NotificatorService) {}


  public postId = null;
  public postTitle = null;
  

  public createNewComment(message: string) {

    this.commentService.createComment(message, this.postId).subscribe(
      (res: any) => {

        this.notificator.success('You have successfully created a comment!');
        this._bottomSheetRef.dismiss(res);
      },
      () => {
        this.notificator.success('The comment failed to be created!');
      }
    );
  }

  public dismiss(): void {
    this._bottomSheetRef.dismiss();
  }

  ngOnInit() {
    this.postId = this.data.postId;
    this.postTitle = this.data.postTitle;
    
  }

  

}





