import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Post } from '../../common/post';
import { MatBottomSheet } from '@angular/material';
import { CommentSheetComponent } from './create-comment/commentSheet/commentSheet.component';
import { PostsService } from 'src/app/core/services/posts.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { VoteType } from 'src/app/common/votes-enum';
import { VotesService } from 'src/app/core/services/votes.service';
import { CommentService } from 'src/app/core/services/comment.service';
import { Comment } from '../../common/comment';
import { UpdatePostSheetComponent } from './update-post/update-post-sheet/update-post-sheet.component';
import { UpdateCommentSheetComponent } from './update-comment/update-comment-sheet/update-comment-sheet.component';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  public post: Post = null;
  public comments: Comment[] = null;
  public postId = null;
  public showComments = false;
  public currentVote = '';
  public showButtonsDeleteAndUpdate = false;



  constructor( private readonly route: ActivatedRoute,
    private _bottomSheet: MatBottomSheet,
    private readonly postsService: PostsService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly storage: StorageService,
    private readonly voteService: VotesService,
    private readonly commentService: CommentService,
    ) {
   }
    ngOnInit() {
      this.route.data.subscribe(
        data => {
          this.post = data.post;
          this.comments = data.comments.sort(function(a, b) { return new Date(b.createdOn).getTime() - new Date(a.createdOn).getTime(); });
        }
      );
      this.postId = this.route.snapshot.params['id'];
      this.postsService.idividualPostVotes(this.postId).subscribe(
        votesData => {
          for (let i in votesData) {
            if (votesData[i].userId === this.storage.get('id')) {
              if (votesData[i].likeOrDislike === 'Like') {
                this.currentVote = 'Like';
              } else {
                this.currentVote = 'Dislike';
              }
              break;
            }
        }
      }
      );

      if (this.storage.get('id') === this.post.authorId || this.storage.get('role') === 'Admin') {
        this.showButtonsDeleteAndUpdate = true;
      }

    }

    showCommentButtonsDeleteAndUpdate(authorId: string) {
      if (this.storage.get('id') === authorId || this.storage.get('role') === 'Admin') {
        return true;
      } else {
        return false;
      }

    }

    public getLikeButtonClass() {
      if (this.currentVote === 'Like') {
        return { buttonLiked: true };
      }
    }

    public getDislikeButtonClass() {
      if (this.currentVote === 'Dislike') {
        return { buttonDisliked: true };
      }
    }

  allComments() {
    this.showComments = true;
}



public openCreateCommentBottomSheet(): void {
  const sheet = this._bottomSheet.open(CommentSheetComponent, {
    panelClass: 'custom-width',
    data: { postId: this.postId },
  });

  sheet.afterDismissed().subscribe( (comment: Comment) => {
    if (comment !== undefined) {
      this.comments.unshift(comment);
    }
  });
}

public openUpdatePostBottomSheet(): void {
  const sheet = this._bottomSheet.open(UpdatePostSheetComponent, {
    panelClass: 'custom-width',
    data: { postId: this.postId, postTitle: this.post.title, postContent: this.post.content },
  });

  sheet.afterDismissed().subscribe( (post: Post) => {
    if (post !== undefined) {
      this.post = post;
    }
  });
}

public openUpdateCommentBottomSheet(commentId: string, message: string): void {
  const sheet = this._bottomSheet.open(UpdateCommentSheetComponent, {
    panelClass: 'custom-width',
    data: { postId: this.postId, commentId, commentMessage: message },
  });

  sheet.afterDismissed().subscribe( (comment: Comment) => {
    if (comment !== undefined) {
      for (let i in this.comments) {
        if (this.comments[i].id === comment.id) {
          this.comments[i].message = comment.message;
           break;
        }
      }
    }
  });
}

public deletePost(): void {
  this.postsService.deletePost(this.postId).subscribe(
    () => {
      this.router.navigate(['/posts']);
      this.notificator.success('Post has been successfully deleted');
    },
    (res) => {
      this.notificator.error(res.error.message);
    }
  );
}

public deleteComment(commentId: string): void {

  this.commentService.deleteComment(commentId, this.postId).subscribe(
    () => {
      this.notificator.success('Comment has been deleted successfully!');
      for (let i in this.comments) {
        if (this.comments[i].id === commentId) {
          this.comments.splice(+i, 1);
           break;
        }
      }
    },
    (res) => {
      this.notificator.error(res.error.message);
    }
  );
}

  vote(likeOrDislike: VoteType) {
    this.voteService.vote(likeOrDislike, this.post.id).subscribe(
      data => {
        if (likeOrDislike === 'Like') {
          if (this.currentVote === 'Dislike') {
            this.currentVote = 'Like';
            this.post.likes++;
            this.post.dislikes--;
          }
          if (this.currentVote === '') {
            this.currentVote = 'Like';
            this.post.likes++;
          }
        }
        if (likeOrDislike === 'Dislike') {
          if (this.currentVote === 'Like') {
            this.currentVote = 'Dislike';
            this.post.likes--;
            this.post.dislikes++;
          }
          if (this.currentVote === '') {
            this.currentVote = 'Dislike';
            this.post.dislikes++;
          }
        }
      }
    );

  }

  commentVote(commentId: string, likeOrDislike: VoteType) {
    this.voteService.commentVote(likeOrDislike, this.postId, commentId).subscribe(
      data => {console.log(data);
      }
    );

  }

  recieveVote($event) {
    this.vote = $event;
  }
}
