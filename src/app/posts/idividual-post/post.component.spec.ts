// /* tslint:disable:no-unused-variable */
// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { DebugElement, inject } from '@angular/core';

// import { PostComponent } from './post.component';
// import { CommonModule, LocationStrategy } from '@angular/common';
// import { SharedModule } from 'src/app/shared/shared.module';
// import { FormsModule } from '@angular/forms';
// import { PostsRoutingModule } from '../posts-routing.module';
// import { MatPaginatorModule, MatSortModule, MatCardModule } from '@angular/material';
// import { PostsComponent } from '../all-posts/posts.component';
// import { SheetComponent } from '../create-post/sheet/sheet.component';
// import { CommentViewComponent } from '../comments/comment-view/comment.view.component';
// import { CommentSheetComponent } from './create-comment/commentSheet/commentSheet.component';
// import { UpdatePostSheetComponent } from './update-post/update-post-sheet/update-post-sheet.component';
// import { UpdateCommentSheetComponent } from './update-comment/update-comment-sheet/update-comment-sheet.component';
// import { ActivatedRoute, Router, ROUTER_CONFIGURATION } from '@angular/router';
// import { HttpClient } from '@angular/common/http';
// import { NotificatorService } from 'src/app/core/services/notificator.service';
// import { RouterTestingModule } from '@angular/router/testing';

// describe('PostComponent', () => {
//   let component: PostComponent;
//   let fixture: ComponentFixture<PostComponent>;
//   const router = jasmine.createSpyObj('Router', ['navigate']);

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [ 
//           PostComponent,
//           PostsComponent,
//           SheetComponent,
//           CommentViewComponent,
//           CommentSheetComponent,
//           UpdatePostSheetComponent,
//           UpdateCommentSheetComponent,
//      ],
//       imports: [
//         CommonModule,
//         SharedModule,
//         FormsModule,
//         PostsRoutingModule,
//         MatPaginatorModule,
//         MatSortModule,
//         MatCardModule,
//         // RouterTestingModule.withRoutes([]),
//       ],
//       providers: [
//           {
//               provide: ActivatedRoute,
//               useValue: {},
//           },
//           {
//             provide: HttpClient,
//             useValue: {},
//           },
//           { provide: Router, 
//             useValue: router 
//           },
//           {
//             provide: NotificatorService,
//             useValue: {},
//           },
//           {
//             provide: LocationStrategy,
//             useValue: {},
//           },
//       ]
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(PostComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//    afterEach(() => {
//     if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
//       (fixture.nativeElement as HTMLElement).remove();
//     }
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
