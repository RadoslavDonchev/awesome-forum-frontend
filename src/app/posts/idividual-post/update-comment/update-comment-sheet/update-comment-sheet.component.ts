import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { CommentService } from 'src/app/core/services/comment.service';


@Component({
  selector: 'app-update-comment-sheet',
  templateUrl: './update-comment-sheet.component.html',
  styleUrls: ['./update-comment-sheet.component.css']
})
export class UpdateCommentSheetComponent implements OnInit {

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  private _bottomSheetRef: MatBottomSheetRef<UpdateCommentSheetComponent>,
  private readonly commentService: CommentService,
  private readonly notificator: NotificatorService) {}

  public updateComment(message) {
    this.commentService.updateComment(message, this.data.postId, this.data.commentId).subscribe(
      (res: any) => {
        this.notificator.success('You have successfully updated the comment!');
        this._bottomSheetRef.dismiss(res);
      },
      () => {
        this.notificator.error('The comment failed to be updated!');
        this._bottomSheetRef.dismiss();
      }
    );
  }

  public dismiss(): void {
    this._bottomSheetRef.dismiss();
  }

  ngOnInit() {
  }

}





