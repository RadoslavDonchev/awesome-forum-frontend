import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './all-posts/posts.component';
import { PostComponent } from './idividual-post/post.component';
import { AuthGuard } from '../auth/auth.guard';
import { PostsResolverService } from './services/posts-resolver.service';
import { PostResolverService } from './services/post-resolver.service';
import { CommentsResolverService } from './services/comments-resolver.service';

const routes: Routes = [
  {path: '', component: PostsComponent, canActivate: [AuthGuard],
    resolve: {posts: PostsResolverService}, pathMatch: 'full'},
  {path: ':id', component: PostComponent, canActivate: [AuthGuard],
    resolve: { post: PostResolverService, comments: CommentsResolverService }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
