import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { PostsService } from 'src/app/core/services/posts.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';


@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.component.html',
  styleUrls: ['./sheet.component.css']
})
export class SheetComponent implements OnInit {

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  private _bottomSheetRef: MatBottomSheetRef<SheetComponent>,
  private readonly postService: PostsService,
  private readonly notificator: NotificatorService) {}

  public createNewPost(title, content) {
    this.postService.createPost(title, content).subscribe(
      (res: any) => {
        this.notificator.success('You have successfully created a post!');
        this._bottomSheetRef.dismiss(res);
      },
      () => {
        this.notificator.error('The post failed to be created!');
        this._bottomSheetRef.dismiss();
      }
    );
  }

  public dismiss(): void {
    this._bottomSheetRef.dismiss();
  }

  ngOnInit() {
  }

}
