import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { UserInfoComponent } from './user-info/component/user-info.component';
import { UserInfoResolverService } from './user-info/services/user-info-resolver.service';

const routes: Routes = [
  {path: ':id', component: UserInfoComponent, canActivate: [AuthGuard],
    resolve: {user: UserInfoResolverService}, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserInfoRoutingModule { }
