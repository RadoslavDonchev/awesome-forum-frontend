import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../../core/services/notificator.service';
import { User } from '../../../common/user';
import { UserService } from '../../../core/services/user.service';
import { StorageService } from '../../../core/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserInfoResolverService implements Resolve<{user: User} | any> {

  constructor(
    private readonly userService: UserService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly storage: StorageService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {

    const id = route.params['id'];

          return this.userService.idividualUser(id).pipe(catchError(
            res => {
              this.notificator.error(res.error.error);
              this.router.navigate(['/home']);
              return of(res);
            }
          ));
        }
  }

