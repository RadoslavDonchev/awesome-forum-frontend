import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserInfoComponent } from './component/user-info.component';
import { SharedModule } from '../../shared/shared.module';
import { UserInfoRoutingModule } from '../user-info-routing.module';

@NgModule({
  declarations: [
    UserInfoComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UserInfoRoutingModule,
  ]
})
export class UserInfoModule { }
