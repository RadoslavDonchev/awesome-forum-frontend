import { Component, OnInit } from '@angular/core';
import { User } from '../../../common/user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { catchError } from 'rxjs/operators';
import { NotificatorService } from '../../../core/services/notificator.service';
import { of, Subscription } from 'rxjs';
import { StorageService } from '../../../core/services/storage.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  user: User = null;
  showFollowButton = true;
  showUnfollowButton = false;
  showBanButton = false;
  showUnbanButton = false;
  showDeleteButton = false;
  private subscription: Subscription;

  constructor( private readonly route: ActivatedRoute,
    private readonly notificator: NotificatorService,
    private readonly userService: UserService,
    private readonly storage: StorageService,
    private readonly router: Router,
    ) {

   }

   public followUser(id: string): void {
    this.userService.followUser(id).pipe(catchError(
      res => {
        this.notificator.error(res.error.error);
        return of(res);
      }
    )).subscribe();
   }

   public unFollowUser(id: string): void {
    this.userService.unFollowUser(id).pipe(catchError(
      res => {
        this.notificator.error(res.error.error);
        return of(res);
      }
    )).subscribe();
   }

   public banUser(id: string): void {
    this.userService.banUser(id, 'just cuz').pipe(catchError(
      res => {
        this.notificator.error(res.error.error);
        return of(res);
      }
    )).subscribe();
   }

   public unBanUser(id: string): void {
    this.userService.unBanUser(id).pipe(catchError(
      res => {
        this.notificator.error(res.error.error);
        return of(res);
      }
    )).subscribe();
   }

   public deleteUser(id: string): void {
    this.userService.deleteUser(id).subscribe(
      () => {
        this.router.navigate(['/home']);
        this.notificator.success('User has been successfully deleted');
      },
      () => {
        this.notificator.error('Failed to delete user');
      }
    );
   }

   ngOnInit() {
    this.route.data.subscribe(
      data => {
        this.user = data.user;
        if (this.user.name === this.storage.get('username')) {
          this.showFollowButton = false;
          this.showUnfollowButton = false;
        }
      }
    );

    if (this.storage.get('role') === 'Admin' && this.user.name !== this.storage.get('username')) {
      this.showBanButton = true;
      this.showUnbanButton = true;
      this.showDeleteButton = true;
    }

    this.subscription = this.userService.userFollowing$.subscribe(
      loggedUser => {
        if (this.user.name === this.storage.get('username')) {
          this.showFollowButton = false;
          this.showUnfollowButton = false;
        } else {
        if (loggedUser.following.__zone_symbol__value.includes(this.user.name)) {
          this.showFollowButton = false;
          this.showUnfollowButton = true;
          if(!this.user.followers.includes(this.storage.get('username'))) {
          this.user.followers.push(this.storage.get('username'));
          }
        } else {
          this.showFollowButton = true;
          this.showUnfollowButton = false;
          const index = this.user.followers.indexOf(this.storage.get('username'));
          if (index !== -1) {
          this.user.followers.splice(index, 1);
          }
        }
      }
      }
    );

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
