export interface Comment {
  id: string;
  author: string;
  authorId: string;
  message: string;
  createdOn: Date;
  updatedOn: Date;
  likes: number;
  dislikes: number;
}
