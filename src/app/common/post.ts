export interface Post {
  id: string;
  title: string;
  content: string;
  author: string;
  authorId: string;
  createdOn: Date;
  updatedOn: Date;
  version: number;
  likes: number;
  dislikes: number;
}
