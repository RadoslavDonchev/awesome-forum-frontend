import { UserRole } from './user-role';

export interface User {
  id: string;
  name: string;
  email: number;
  followers: string[];
  following: string[];
  role: UserRole;
}
