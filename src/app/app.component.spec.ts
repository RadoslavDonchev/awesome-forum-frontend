import { HttpClient } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from './shared/shared.module';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NotificatorService } from './core/services/notificator.service';
import { AuthService } from './core/services/auth.service';
import { of } from 'rxjs';
import { PostsModule } from './posts/posts.module';
import { UserInfoModule } from './modules/user-info/user-info.module';
import { APP_BASE_HREF } from '@angular/common';

describe('AppComponent', () => {
  let fixture;
  let notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  let auth = jasmine.createSpyObj('AuthService', ['login', 'logout', 'register']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        NavbarComponent,
      ],
      imports: [
        SharedModule,
        AppRoutingModule,
        PostsModule,
        UserInfoModule,
      ],
      providers: [
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: AuthService,
          useValue: auth,
        },
        {provide: APP_BASE_HREF, useValue : '/' }
      ]
    });
  }));

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize with the correct logged user data', async() => {
    auth.user$ = of({username: 'testName', id: 'testId'});
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    await fixture.detectChanges();
    expect(app.username).toBe('testName');
    expect(app.id).toBe('testId');
  });

  it('should initialize with empty username when the user is logged out', async() => {
    auth.user$ = of({username: null, id: null});
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    await fixture.detectChanges();
    expect(app.username).toBe('');
  });

    // describe('logout', () => {
    //   it('should call auth.logout() once', () => {
    //     const service: AuthService = TestBed.get(AuthService);

    //     auth.logout.calls.reset();
    //     service.login('name2', 'password2').subscribe(
    //       () => expect(http.post).toHaveBeenCalledTimes(1)
    //     );
    //   });

    // });

  });
