ng g m core
ng g s core/services/storage --flat --skipTests
ng g s core/services/movies --flat --skipTests
ng g s core/services/notificator --flat --skipTests
ng g s core/services/auth --flat --skipTests
ng g c components/home --skipTests
ng g c components/login --skipTests
ng g c components/navbar --skipTests
ng g c components/register --skipTests
ng g c components/siderbar --skipTests
ng g m shared
ng g g auth/auth --skipTests
ng g s auth/token-interceptor --skipTests
ng g m movies
ng g c movies --skipTests
ng g s movies/services/movies-resolver --skipTests